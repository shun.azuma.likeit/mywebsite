package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserInfo;
import dao.MyUserDAO;

/**
 * Servlet implementation class Register_Post
 */
@WebServlet("/Register_Post")
public class Register_Post extends HttpServlet {
	private static final long serialVersionUID = 1L;



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher(ECHelper.REGIST_RESULT_PAGE).forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {
			String inputLoginId = request.getParameter("login_id");
		    String inputUserName = request.getParameter("user_name");
			String inputPassword = request.getParameter("password");

			UserInfo udb = new UserInfo();
		    udb.setLoginId(inputLoginId);
			udb.setName(inputUserName);
			udb.setPassword(inputPassword);


			MyUserDAO.insertUser(udb);
			request.setAttribute("udb", udb);
			request.getRequestDispatcher(ECHelper.REGIST_RESULT_PAGE).forward(request, response);

//			// 登録が確定されたかどうか確認するための変数
//			String confirmed = request.getParameter("confirm_button");
//
//			switch (confirmed) {
//			case "cancel":
//				session.setAttribute("udb", udb);
//				response.sendRedirect("Register_Post");
//				break;
//
//			case "regist":
//				MyUserDAO.insertUser(udb);
//				request.setAttribute("udb", udb);
//				request.getRequestDispatcher(ECHelper.REGIST_RESULT_PAGE).forward(request, response);
//				break;
//			}

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("Register_Post");
		}
	}









	}

