package beans;

import java.io.Serializable;

/**
 * アイテム
 * @author d-yamaguchi
 *
 */
public class ItemDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String category;
	private String fileName;
	private int stock;
	
	private int selectprice;
	private int selectCount;





	public int getSelectprice() {
		return selectprice;
	}
	public void setSelectprice(int selectprice) {
		this.selectprice = selectprice;
	}
	public int getSelectCount() {
		return selectCount;
	}
	public void setSelectCount(int selectCount) {
		this.selectCount = selectCount;
	}
	public int getId() {
		return id;
	}
	public void setId(int itemId) {
		this.id = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String itemName) {
		this.name = itemName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int itemPrice) {
		this.price = itemPrice;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String filename) {
		this.fileName = filename;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

	//追加したもの

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}

	 //商品単価と選んだ数を掛ける

		public int getStockprice(ItemDataBeans items, int select) {
			int totaltwo = 0;

	    	totaltwo=totaltwo*select;

	    	return totaltwo;
	    }

}

